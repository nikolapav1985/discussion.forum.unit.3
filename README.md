Discussion forum unit 3 example
--------------------------------

- chained.py (chained conditional statements example)
- nested.py (nested conditional statements example)
- nested.straight.py (nested conditional statements made straight example)

Run examples
-------------

- ./chained.py 0 180 (or some other values, check comments for details)
- ./nested.py 0 180
- ./nested.straight.py 0 180

Test environment
----------------

OS lubuntu 16.04 lts 64 bit kernel version 4.13.0
