#!/usr/bin/python

"""
FILE nested.py

Example of nested conditional statements.

TESTS

INPUT 0 180

OUTPUT

3.14159265359

INPUT 1 3.14

OUTPUT

179.908747671

INPUT 2 3.14

OUTPUT

No choice given

INPUT 3.14

OUTPUT

Nothing, script requires exactly three arguments to run.

Chained CONDITIONALS support three options in this case. 0 - convert a value to radians, 1 - convert a value to degrees, anything else - not a valid choice, print a message in this case.

"""

import math
import sys

def toRad(n):
    """
        procedure toRad

        convert a value to radians pi rad = 180 deg

        parameters

        n(real) a number to convert to radians
    """
    return (n*math.pi)/180

def toDeg(n):
    """
        procedure toDeg

        convert a value to degrees pi rad = 180 deg

        parameters

        n(real) a number to convert to degrees
    """
    return (n*180)/math.pi

if __name__=="__main__":
    if len(sys.argv) == 3:
        choice=int(sys.argv[1])
        item=float(sys.argv[2])
        if choice == 0:
            print(toRad(item))
        elif choice == 1:
            print(toDeg(item))
        else:
            print("No choice given")
